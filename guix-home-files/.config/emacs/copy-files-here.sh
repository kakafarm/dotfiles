#!/bin/bash

cp ~/.config/emacs/init.el \
   ~/.config/emacs/elfeed-feeds.el \
   .
cp ~/.config/emacs/local-packages/emacs-kakafarm/kakafarm.el \
   local-packages/emacs-kakafarm/
